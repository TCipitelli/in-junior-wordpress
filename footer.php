<footer> 
      <div class="map">
        <iframe width="250px" height="170px" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.192670444559!2d-43.13629768503422!3d-22.906263285012148!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817ee1756031%3A0xd1dcbde0df6f873c!2sAv.%20Milton%20Tavares%20de%20Souza%20-%20Boa%20Viagem%2C%20Niter%C3%B3i%20-%20RJ%2C%2024210-346!5e0!3m2!1spt-BR!2sbr!4v1645357017398!5m2!1spt-BR!2sbr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        <div class="contact">
          <div class="end"><i class="bi bi-geo-alt-fill"></i> Av. Milton Tavares de Souza, s/n - Sala 115 B - Boa<br>Viagem, Niterói - RJ, 24210-315</div>
          <div><i class="bi bi-telephone-fill"></i> (99) 99999-9999</div>
          <div><i class="bi bi-envelope-fill"></i> salve-lobos@lobINhos.com</div>
          <button class="more">Quem Somos</button>
        </div>
      </div>
      <div class="dev-logo">
        Desenvolvido com
        <img src="<?php echo get_stylesheet_directory_uri()?>./assets/paws.svg" alt="">
      </div>
    </footer>
    <script src="./js/exemple.js"></script>
    <?php wp_footer();?>
  </body>
</html>
