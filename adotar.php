<?php
// Template Name: adotar
?>
<?php get_header();?>
    <section id="section">
      <form name="form">
        <div class="line">
          <div class="nome">
            <label for="nome">Seu Nome:</label><br>
            <input  type="text" name="nome" value="">
          </div>
          <div class="idade">
            <label for="idade">Idade:</label><br>
            <input maxlength="3" size="2" type="text" name="idade" value="">
          </div>
        </div>
        <div class="line">
          <div class="mail">
            <label for="e-mail">E-mail:</label><br>
            <input type="text" name="e-mail" value="">
          </div>
        </div>
        <button class="btn" onclick="postWolf()">Adotar</button>
      </form>
    </section>
<?php get_footer();?>