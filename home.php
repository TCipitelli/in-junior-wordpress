<?php
// Template Name: lista
?>
<?php get_header();?>
    <section>
      <div class="menu">
        <div class="search">
          <input class="search-bar" type="text" name="nome" value="">
          <button class="go" onclick="search()">+ Lobo</button>
        </div>
        <input type="checkbox" id="chkbox" value="">
        Ver lobinhos adotados
      </div>
      <div class="wolves">
          <div class="data">
            <a class="btn" href="./show-lobo.html">Adotar</a>
            <div class="name">
            <h4><?php the_field('titulo');?></h4>
              Idade: <?php the_field('idade');?> anos
            </div>
          </div>
          <p><?php the_field('descricao');?></p>
          </div>
          <div class="box-bg-right"></div>
          <?php if( get_field('foto do lobo'));?>
          <div class="image" style="background-image: url('?php the_field('foto');?>')"></div>
          <?php endif; ?>
      </div>
      <div class="wolves">
          <div class="box-bg"></div>
          <?php if( get_field('foto do lobo'));?>
          <div class="image" style="background-image: url('?php the_field('foto');?>')"></div>
          <?php endif; ?>
          <div class="info">
            <div class="data">
              <div class="name">
                <h4><?php the_field('titulo');?></h4>
                Idade: <?php the_field('idade');?> anos
              </div>            </div>
            <p><?php the_field('descricao');?></p>
          </div>
      </div>
      <div class="pages">
        <a href="#" onclick="alerta()"><<</a>
        <a href="#" onclick="listWolves(1)">1</a>
        <a href="#" onclick="listWolves(2)">2</a>
        <a href="#" onclick="listWolves(3)">3</a>
        <a href="#" onclick="listWolves(1)">...</a>
        <a href="#" onclick="listWolves(1)">>></a>
      </div>
    </section>
<?php get_footer();?>
