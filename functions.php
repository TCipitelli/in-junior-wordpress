<?php
function console_log( $data ){
    echo '<script>';
    echo 'console.log('. json_encode( $data ) .')';
    echo '</script>';
}
  function my_theme_styles_function(){
    //console_log("styles");
    global $template;
    wp_enqueue_style( 'reset', get_template_directory_uri() . '/css/reset.css');
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');
    //adicionar
    basename($template)=="adicionar.php"?wp_enqueue_style( 'adicionar', get_template_directory_uri() . '/css/adicionar.css'):0;
    basename($template)=="adotar.php"?wp_enqueue_style( 'adotar', get_template_directory_uri() . '/css/adotar.css'):0;
    if (basename($template)=="index.php"){wp_enqueue_style( 'index', get_template_directory_uri() . '/css/index.css');};
    basename($template)=="lista.php"?wp_enqueue_style( 'lista', get_template_directory_uri() . '/css/lista.css'):0;
  }
  add_action('wp_enqueue_scripts', 'my_theme_styles_function');
?>