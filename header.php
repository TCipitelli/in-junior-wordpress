<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Adote um Lobo</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Darker+Grotesque:wght@400;700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css"rel="stylesheet">
    <?php wp_head();?>
  </head>
  <body>
    <header>
      <a class="effect lft" href="./lista-lobos.html">Nossos Lobinhos</a>
      <img src="<?php echo get_stylesheet_directory_uri()?>./assets/Group 3.svg" alt="">
      <a class="effect rgt" href="#">Quem Somos</a>
    </header>