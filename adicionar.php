<?php
// Template Name: adicionar
?>
<?php get_header();?>
    <section>
      <form name="form" onsubmit="envia()">
        <h1>Coloque um Lobinho para Adoção</h1>
        <div class="line">
          <div class="nome">
            <label for="nome">Nome do Lobinho:</label><br>
            <input  type="text" name="nome" value="">
          </div>
          <div class="idade">
            <label for="idade">Anos:</label><br>
            <input maxlength="3" size="2" type="number" name="idade" value="">
          </div>
        </div>
        <div class="line">
          <div class="link">
            <label for="">Link da Foto:</label><br>
            <input type="" name="foto" value="">
          </div>
        </div>
        <div class="line">
            <div class="desc">
              <label for="">Descrição:</label><br>
              <textarea rows="5" col="100%" name="descr"></textarea>
            </div>
        </div>
        <button class="btn" type="submit">Salvar</button>
      </form>
    </section>
<?php get_footer();?>
